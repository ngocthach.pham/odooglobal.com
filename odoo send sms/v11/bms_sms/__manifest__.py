# -*- coding: utf-8 -*-

{
    'name': 'BMS SMS MySMSBrandname Demo',
    'summary': 'BMS SMS MySMSBrandname Demo',
    'version': '10.0.1.0.0',
    'author': 'BMS',
    'category': 'BMS Application',
    'license': 'AGPL-3',
    'website': 'http://www.bmstech.io',
    'sequence': 1,
    'depends': ['base','sale'],
    'data': [
        "views/bms_sms.xml"
    ],
    'demo': [
    ],
    'css': [],
    'images': [
        'static/description/icon.png',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'description': """
		Demo Gửi SMS tới khách hàng
	""",
}
