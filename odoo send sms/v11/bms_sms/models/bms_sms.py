# -*- coding: utf-8 -*-

from odoo import fields, models, api
import xmlrpc.client


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.one
    def send_sms(self):

        # print("Hello SMS")

        url = 'http://mysmsbrandname.com'
        db = 'sms_bms'
        username = 'cs'
        password = '1'

        common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
        models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
        uid = common.authenticate(db, username, password, {})

        # Data input
        title = 'xmlrpc'
        brandname = 'SoLienLacDT'
        accent = 'False'
        content = 'Cam on khach hang '+self.partner_id.name+ ' da mua don hang tri gia '+ str(self.amount_total)
        # phone = '84963523434'
        phone = self.partner_id.mobile

        # Call via xmlrpc
        models.execute_kw(db, uid, password, 'bms.send_sms_xmlrpc', 'create', [
            {'title': title,
             'brandname': brandname,
             'accent': accent,
             'content': content,
             'phone': phone}])